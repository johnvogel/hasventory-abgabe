module Handler.Borrowers where

import Import
import Database.Persist.Sqlite


-- GET Handler to show a list of all non-archived borrowers.
getBorrowersR :: Handler Html
getBorrowersR = do
    borrowers       <- runDB $ selectList [BorrowerBorrowerArchived ==. False] []
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Borrowers
            <a href="/add/borrower" class="options-link">+ add borrower
            <table>
                <tr>
                    <th>ID
                    <th>Name
                    <th>E-Mail
                    <th>Actions
                $forall (Entity borrowerKey borrower) <- borrowers
                    <tr>
                        <td><span>#{show $ unSqlBackendKey $ toBackendKey borrowerKey}</span>
                        <td><span>#{borrowerName borrower}</span>
                        <td><span>#{borrowerEmail borrower}</span>
                        <td><ul class="horizontal-list table-row-options">
                            <li><a href="/borrower/#{show $ unSqlBackendKey $ toBackendKey borrowerKey}"><img src="/static/img/info-icon.png" alt="details" title="details"></a>
                            <li><a href="/edit/borrower/#{show $ unSqlBackendKey $ toBackendKey borrowerKey}"><img src="/static/img/pencil-icon.png" alt="edit" title="edit"></a>
                            <li><a href="/archive/borrower/#{show $ unSqlBackendKey $ toBackendKey borrowerKey}"><img src="/static/img/trash-can-icon.png" alt="archive" title="archive"></a>
        |]

