module Handler.AddMultiRentalAction where

import Import
import Data.List (head)
import Data.Time
import Data.Text (pack, unpack)
import Database.Persist.Sqlite
import System.IO.Unsafe
import System.Locale (defaultTimeLocale)
import qualified Data.Time.Format (parseTime)


-- POST Handler to receive data from "lendItemForm". Gets multiple items through
-- additional POST parameters "item"
postAddMultiRentalActionR :: Handler Html
postAddMultiRentalActionR = do
    borrowers <- runDB $ selectList [] []
    itemIDs <- lookupPostParams "item"
    ((result, _), _) <- runFormPost (lendItemForm (map unpack itemIDs) borrowers)
    case result of
        FormSuccess rentalAction ->  do 
                                        _ <- addRentalActionToDatabase rentalAction
                                        formSuccess
        FormFailure _            ->  do
                                        formFailure
        _                        ->  do
                                        formFailure


-- Success message
formSuccess :: Handler Html
formSuccess = do
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Lend items
            <a href="/items/" class="options-link">&#8592; back
            <pre class="success">Items lent!
        |]


-- Failure message
formFailure :: Handler Html
formFailure = do
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Lend items
            <a href="/items/" class="options-link">&#8592; back
            <pre class="failure">Error: Items not lent!
        |]


-- Function to add RentalAction to the database and update the reference to
-- the latestRentalAction in the item
addRentalActionToDatabase :: RentalAction -> Handler (Key RentalAction)
addRentalActionToDatabase rentalAction = runDB $ do
    rentalActionKey <- insert rentalAction
    updateWhere [ItemId <-. (rentalActionItems rentalAction)] [ItemLatestRentalAction =. (Just rentalActionKey)]
    return rentalActionKey


-- Applicative Form to lend one or more items.
lendItemForm :: [String] -> [Entity Borrower] -> Form RentalAction
lendItemForm itemIDs borrowers = renderDivs $ RentalAction
    <$> pure (map ItemKey (map SqlBackendKey (map abs (map read itemIDs))))
    <*> pure True
    <*> areq (selectFieldList $ borrowerList borrowers) "Borrower*:" (Just $ entityKey $ head borrowers)
    <*> pure (unsafePerformIO getCurrentTime)
    <*> aopt utcTimeField "Return date* (dd.mm.yyyy):" (Just $ Just $ addUTCTime 86400 (unsafePerformIO getCurrentTime))
    <*> aopt textareaField "Comment:" Nothing
    <*> aopt checkBoxField "Permanent Rental:" Nothing

    where
        -- Creates a list of tuples consisting of the name and the key of the borrower
        borrowerList :: [Entity Borrower] -> [(Text, Key Borrower)]
        borrowerList entityBorrowers = do
            map (\borrower -> (borrowerName $ entityVal borrower, entityKey borrower)) entityBorrowers

        -- Defines a custom field that accepts a date
        utcTimeField :: Monad m => RenderMessage (HandlerSite m) FormMessage => Field m UTCTime
        utcTimeField = Field
             { fieldParse = parseHelper parseTime'
             , fieldView = \theId name attrs val isReq -> toWidget [hamlet|
        $newline never
        <input id="#{theId}" name="#{name}" type="datetime" *{attrs} :isReq:required="" value="#{showVal val}">
        |]
             , fieldEnctype = UrlEncoded
             }
            where
               showVal = either id (pack . formatTime defaultTimeLocale "%d.%m.%Y")

        -- Parses the planedReturnDate toa UTCTime
        parseTime' :: Text -> Either FormMessage UTCTime
        parseTime' theText = 
            maybe 
               (Left MsgInvalidTimeFormat) 
               (\x -> Right x) 
               (Data.Time.Format.parseTime defaultTimeLocale "%d.%m.%Y" $ unpack theText)