module Handler.GetBorrower where

import Import
import Database.Persist.Sqlite


-- API endpoint that returns a borrower as JSON
getGetBorrowerR :: String -> Handler Value
getGetBorrowerR borrowerID = do
    maybeBorrower <- runDB $ get $ BorrowerKey $ SqlBackendKey $ abs (read borrowerID)
    case maybeBorrower of
        Nothing         ->  sendResponseStatus status404 ("ID NOT VALID" :: Text)
        Just borrower   ->  return $ toJSON borrower
