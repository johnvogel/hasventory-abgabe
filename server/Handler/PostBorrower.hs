module Handler.PostBorrower where

import Import
import qualified Data.Aeson as J
import Database.Persist.Sqlite


-- API Endpoint to add a borrower, returns the ID of the new borrower
postPostBorrowerR :: Handler Value
postPostBorrowerR = do
    result <- parseJsonBody :: Handler (J.Result Borrower)
    case result of
        J.Error _           -> sendResponseStatus status400 ("JSON NOT VALID" :: String)
        J.Success borrower  -> postPostRentalActionR' borrower


-- Continuation of postPostRentalActionR
postPostRentalActionR' :: Borrower -> Handler Value
postPostRentalActionR' borrower = do
    borrowerID <- addBorrowerToDatabase borrower
    sendResponseStatus status200 ((show $ unSqlBackendKey $ toBackendKey borrowerID):: String)


-- Adds the Borrower to the database
addBorrowerToDatabase :: Borrower -> Handler (Key Borrower)
addBorrowerToDatabase borrower = runDB $ do
    borrowerID <- insert borrower
    return borrowerID