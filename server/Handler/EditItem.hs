module Handler.EditItem where

import Import
import Database.Persist.Sqlite


-- GET Handler to show the editItem form.
getEditItemR :: String -> Handler Html
getEditItemR itemID = do
    item <- getItem itemID
    (formWidget, formEnctype) <- generateFormPost (editItemForm item)
    if(itemItemArchived item) then
        cannotEditItem
        else
            defaultLayout $ do
                setTitle "Hasventory"
                [whamlet|
                    <h2>Edit Item
                    <a href="/items/" class="options-link">&#8592; back
                    <form method=post action="/edit/item/#{itemID}" enctype=#{formEnctype}>
                        ^{formWidget}
                        <button>Edit Item
                |]


-- Applicative form to edit the item. Is prefilled with the data
-- from the selected item
editItemForm :: Item -> Form Item
editItemForm item = renderDivs $ Item
        <$> areq textField "Manufacturer*:" (Just $ itemManufacturer item)
        <*> areq textField "Model*:" (Just $ itemModel item)
        <*> areq (selectFieldList types) "Type*:" (Just $ itemItemType item)
        <*> aopt textField "IMEI/Serial number:" (Just $ itemSerialNumber item)
        <*> aopt textField "Old Item ID:" (Just $ itemOldItemID item)
        <*> aopt textareaField "Comment:" (Just $ itemItemComment item)
        <*> areq checkBoxField "Marketing device:" (Just $ itemMarketingItem item)
        <*> pure (itemLatestRentalAction item)
        <*> pure (itemItemArchived item)
        where
            types :: [(Text, Text)]
            types = [("Smartphone", "Smartphone")
                    , ("Featurephone", "Featurephone")
                    , ("Tablet", "Tablet")
                    , ("Surfstick", "Surfstick")
                    , ("Laptop", "Laptop")
                    , ("Zubehör", "Zubehör")
                    , ("Sonstiges", "Sonstiges")]


-- Gets the item from the database
getItem :: String -> Handler (Item)
getItem itemID = do
    runDB $ get404 $ ItemKey $ SqlBackendKey $ abs (read itemID)


-- POST Handler that shows a succes or error message
postEditItemR :: String -> Handler Html
postEditItemR itemID = do
    item <- getItem itemID
    ((result, _), _) <- runFormPost (editItemForm item)
    case result of
        FormSuccess formItem    ->  do 
                                    _ <- updateItemInDatabase itemID formItem
                                    return ()
        _                   ->  return ()
    insertedItem <- getItem itemID
    (formWidget, formEnctype) <- generateFormPost (editItemForm insertedItem)
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Edit Item
            <a href="/items/" class="options-link">&#8592; back
            <pre class="success">Edited Item saved!
            <form method=post action="/edit/item/#{itemID}" enctype=#{formEnctype}>
                ^{formWidget}
                <button>Edit Item
        |]


-- Updates the item by replacing the entity in the database
updateItemInDatabase :: String -> Item -> Handler ()
updateItemInDatabase itemID item = do
    runDB $ do
        replace (ItemKey $ SqlBackendKey $ abs (read itemID)) item
        return ()


-- Shows an error message that the item can't be deleted because it is archived.
cannotEditItem :: Handler Html
cannotEditItem = defaultLayout $ do
    setTitle "Hasventory"
    [whamlet|
        <h2>Edit Item
        <a href="/items/" class="options-link">&#8592; back
        <pre class="failure">Error: This item is archived!
    |]