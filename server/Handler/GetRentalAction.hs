module Handler.GetRentalAction where

import Import
import Database.Persist.Sqlite


-- API endpoint that returns a rentalAction as JSON
getGetRentalActionR :: String -> Handler Value
getGetRentalActionR rentalActionID = do
    maybeRentalAction <- runDB $ get $ RentalActionKey $ SqlBackendKey $ abs (read rentalActionID)
    case maybeRentalAction of
        Nothing             ->  sendResponseStatus status404 ("ID NOT VALID" :: Text)
        Just rentalAction   ->  return $ toJSON rentalAction