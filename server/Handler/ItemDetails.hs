module Handler.ItemDetails where

import Import
import Data.List
import Data.Text (unpack, pack)
import Data.Time.Clock
import Data.Time.Format
import Database.Persist.Sqlite
import System.Locale


-- GET Handler that shows detailed information for an item. It displays all item
-- related information like Model name or IMEI. It also shows the rental history of the item.
getItemDetailsR :: String -> Handler Html
getItemDetailsR itemID = do
    item                <- runDB $ get404 $ ItemKey $ SqlBackendKey $ abs (read itemID)
    entityRentalActions <- runDB $ selectList 
        []
        [Desc RentalActionId]
    entityInventoryItems <- runDB $ selectList 
        [InventoryItemInventoryItem ==. (ItemKey $ SqlBackendKey $ abs (read itemID))]
        [Desc InventoryItemDate]
    entityBorrowers     <- runDB $ selectList [] []
    
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Item details
            <a href="/items/" class="options-link">&#8592; back
            <table>
                <tr>
                    <th colspan="2">#{itemManufacturer item} #{itemModel item} <span class="archived">#{showArchived item}</span>
                $if (itemMarketingItem item)
                    <tr>
                        <td colspan="2">Markting device
                <tr>
                    <td>ID
                    <td>#{itemID}
                $maybe oldItemID <- (itemOldItemID item)
                    <tr>
                        <td>Old Item ID
                        <td>#{unpack oldItemID}
                $maybe serialNumber <- (itemSerialNumber item)
                    <tr>
                        $if (((itemItemType item) == "Smartphone") || ((itemItemType item) == "Featurephone"))
                            <td>IMEI
                        $else
                            <td>Serial Number
                        <td>#{unpack serialNumber}
                <tr>
                    <td>Type
                    <td>#{itemItemType item}
                $maybe comment <- (itemItemComment item)
                    <tr>
                        <td>Comment
                        <td>#{unTextarea comment}
            <br />
            <h2>Rental history
            <table class="rentalhistory">
                <tr>
                    <th>Rental ID
                    <th>Action
                    <th>Borrower
                    <th>Date
                    <th>Planned Return
                    <th>Comment
                $forall (Entity rentalActionKey rentalAction) <- (cleanList entityRentalActions (ItemKey $ SqlBackendKey $ abs (read itemID)))
                    <tr>
                        <td>#{show $ unSqlBackendKey $ toBackendKey rentalActionKey}
                        <td>#{rentalType $ rentalActionLent rentalAction}
                        <td>#{getBorrowerName entityBorrowers (rentalActionBorrower rentalAction)}
                        <td>#{formatTime defaultTimeLocale "%d.%m.%Y" $ rentalActionActionDate rentalAction}
                        <td>#{maybe "-" formatReturnDate (rentalActionPlannedReturnDate rentalAction)}
                        <td>#{maybe "-" unTextarea (rentalActionRentalComment rentalAction)}
            <br />
            <h2>Stock-taking history
            <table>
                <tr>
                    <th>ID
                    <th>Date
                    <th>Run-Through
                    <th>Status
                $forall (Entity inventoryItemKey inventoryItem) <- entityInventoryItems
                    <tr>
                        <td>#{show $ unSqlBackendKey $ toBackendKey inventoryItemKey}
                        <td>#{formatTime defaultTimeLocale "%d.%m.%Y" $ inventoryItemDate inventoryItem}
                        <td>#{show $ unSqlBackendKey $ toBackendKey (inventoryItemRunThrough inventoryItem)}
                        <td>in stock
        |]


-- Returns a Text that describes if the item has benn archived or not
showArchived :: Item -> Text
showArchived item
    | (itemItemArchived item) = "(item archived)"
    | otherwise               = ""


-- Returns a Text that describes if the rentalAction was to borrow or return an item.
rentalType :: Bool -> Text
rentalType True  = "lent"
rentalType False = "returned"


-- Returns a Text that describes if the item is a marketing item or not
marketingItem :: Bool -> Text
marketingItem True  = "yes"
marketingItem False = "no"


-- Gets a list of all borrowers and a Borrower Key and returns the name of the borrower
getBorrowerName :: [Entity Borrower] -> Key Borrower -> Text
getBorrowerName entityBorrowers borrowerKey =
    getBorrowerName' (filter (\x -> entityKey x == borrowerKey) entityBorrowers) borrowerKey


-- Continuation of getBorrowerName
getBorrowerName' :: [Entity Borrower] -> Key Borrower -> Text
getBorrowerName' [] key = ("Borrower " <> (pack $ show $ unSqlBackendKey $ toBackendKey key) <> " (deleted)")
getBorrowerName' x  _   = borrowerName $ entityVal $ head x


-- Formats the returnDate
formatReturnDate :: UTCTime -> String
formatReturnDate dateTime =
    formatTime defaultTimeLocale "%d.%m.%Y" dateTime


-- Cleans the list of rentalAction to only contain RentalActions that also contain the itemID
cleanList :: [Entity RentalAction] -> Key Item -> [Entity RentalAction]
cleanList [] _             = []
cleanList [x] y
    | (containsItemId x y) = x : cleanList [] y
    | otherwise            = cleanList [] y
cleanList (x:xs) y
    | (containsItemId x y) = x : cleanList xs y
    | otherwise            = cleanList xs y


-- Return true, if the RentalAction contains an item with the specified Key
containsItemId :: Entity RentalAction -> Key Item -> Bool
containsItemId (Entity _ rentalAction) itemKey =
    itemKey `elem` (rentalActionItems rentalAction)