module Handler.ReturnItem where

import Import
import Data.Time.Clock
import System.IO.Unsafe
import Database.Persist.Sqlite


-- POST Handler that creates a new RentalAction to return an item
postReturnItemR :: String -> String -> Handler Html
postReturnItemR itemID borrowerID = do
    _ <- addRentalActionToDatabase $ RentalAction
        [(ItemKey $ SqlBackendKey $ abs (read itemID))]
        False
        (BorrowerKey $ SqlBackendKey $ abs (read borrowerID))
        (unsafePerformIO getCurrentTime)
        Nothing
        Nothing
        Nothing
    
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Return item
            <a href="/items/" class="options-link">&#8592; back
            <pre class="success">Item returned!
        |]


-- Adds the RentalAction to the database
addRentalActionToDatabase :: RentalAction -> Handler (Key RentalAction)
addRentalActionToDatabase rentalAction = runDB $ do
    rentalActionKey <- insert rentalAction
    updateWhere [ItemId <-. (rentalActionItems rentalAction)] [ItemLatestRentalAction =. (Just rentalActionKey)]
    return rentalActionKey