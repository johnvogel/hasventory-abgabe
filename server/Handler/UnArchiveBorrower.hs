module Handler.UnArchiveBorrower where

import Import
import Database.Persist.Sqlite


-- GET Handler to show a form to unarchive a borrower
getUnArchiveBorrowerR :: String -> Handler Html
getUnArchiveBorrowerR borrowerID = do
    borrower <- runDB $ get404 $ BorrowerKey $ SqlBackendKey $ abs (read borrowerID)
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Unarchive Borrower
            <a href="/archive" class="options-link">&#8592; back
            <form method="post" action="/unarchive/borrower/#{borrowerID}">
                Do you want to unarchive "#{borrowerName borrower}" (ID: #{borrowerID})?<br /><br />
                <button>unarchive
        |]


-- POST Handler that unarchives the borrower by updating the attribute
-- of the borrower in the database
postUnArchiveBorrowerR :: String -> Handler Html
postUnArchiveBorrowerR borrowerID = do
    runDB $ update (BorrowerKey $ SqlBackendKey $ abs (read borrowerID)) [BorrowerBorrowerArchived =. False]
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Unarchive Borrower
            <a href="/archive" class="options-link">&#8592; back
            <pre class="success">Borrower unarchived!
        |]
