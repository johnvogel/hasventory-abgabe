module Handler.GetItemStatus where

import Import
import Database.Persist.Sqlite


-- API endpoint that returns the rental status of an item
getGetItemStatusR :: String -> Handler ()
getGetItemStatusR itemID = do
    maybeItem <- runDB $ get $ ItemKey $ SqlBackendKey $ abs (read itemID)
    case maybeItem of
        Nothing     ->  sendResponseStatus status404 ("ID NOT VALID" :: Text)
        Just item   ->  getGetItemStatusR' item


-- If there is no latestRentalAction, the items is not borrowed
getGetItemStatusR' :: Item -> Handler ()
getGetItemStatusR' item = do
    case (itemLatestRentalAction item) of
        Nothing                 -> sendResponseStatus status200 ("NOT BORROWED" :: Text)
        Just rentalActionKey    -> getGetItemStatusR'' rentalActionKey


-- A latestRentalAction should always exist, so this error message is just there for
-- safety reasons
getGetItemStatusR'' :: Key RentalAction -> Handler ()
getGetItemStatusR'' rentalActionKey = do
    maybeRentalAction <- runDB $ get $ rentalActionKey
    case maybeRentalAction of
        Nothing             -> sendResponseStatus status404 ("ID NOT VALID" :: Text)
        Just rentalAction   -> getGetItemStatusR''' rentalAction


-- Returns the rental status of the latestRentalAction
getGetItemStatusR''' :: RentalAction -> Handler ()
getGetItemStatusR''' rentalAction =
    if(rentalActionLent rentalAction) then
        sendResponseStatus status200 ("BORROWED" :: Text)
    else
        sendResponseStatus status200 ("NOT BORROWED" :: Text)