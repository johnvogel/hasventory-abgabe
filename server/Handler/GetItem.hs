module Handler.GetItem where

import Import
import Database.Persist.Sqlite


-- API endpoint that returns an item as JSON
getGetItemR :: String -> Handler Value
getGetItemR itemID = do
    maybeItem <- runDB $ get $ ItemKey $ SqlBackendKey $ abs (read itemID)
    case maybeItem of
        Nothing 	-> 	sendResponseStatus status404 ("ID NOT VALID" :: Text)
        Just item 	-> 	return $ toJSON item