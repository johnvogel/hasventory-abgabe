{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Control.Applicative
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Resource (runResourceT)
import Data.Time
import Data.Char
import Data.Text hiding (head)
import Data.Aeson
import Data.Maybe
import Data.Either
import Data.Conduit
import Data.Word8
import qualified Data.Aeson as JSON
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Char8 as B8
import GHC.Generics
import System.Locale
import System.Exit
import System.Cmd( system )
import Network.Curl
import Network.URI
import Network.HTTP
import Network.HTTP.Headers
import Network.HTTP.Base
import Network.HTTP.Conduit (simpleHttp)
import Network.HTTP.Conduit -- the main module

data Item         = Item         { manufacturer       :: String
                                 , model              :: String
                                 , itemType           :: String
                                 , serialNumber       :: Maybe String
                                 , oldItemID          :: Maybe String
                                 , itemComment        :: Maybe String
                                 , marketingItem      :: Bool
                                 , latestRentalAction :: Maybe Integer
                                 , itemArchived       :: Bool
                                 } deriving (Show, Generic)

data Borrower     = Borrower     { name             :: String
                                 , email            :: String
                                 , borrowerComment  :: Maybe String
                                 , borrowerArchived :: Bool
                                 } deriving (Show, Generic)

data RentalAction = RentalAction { items             :: [Integer]
                                 , lent              :: Bool
                                 , borrower          :: Integer
                                 , actionDate        :: UTCTime
                                 , plannedReturnDate :: Maybe UTCTime
                                 , rentalComment     :: Maybe String
                                 , permanentRental   :: Maybe Bool
                                 } deriving (Show, Generic)
instance FromJSON Item
instance FromJSON Borrower
instance FromJSON RentalAction
instance ToJSON Item
instance ToJSON Borrower
instance ToJSON RentalAction


-- | This is a test annotation
main = do
            putStrLn (  "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" ++ 
                        "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" ++
                        "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" ++
                        "   ____  ___)                               \n" ++                
                        "  (, /   /                                  \n" ++
                        "    /---/  _   _  _ _  _ __  _/_ _____      \n" ++
                        " ) /   (__(_(_/_)_(/__(/_/ (_(__(_)/ (_(_/_ \n" ++
                        "(_/                                   .-/   \n" ++
                        "                                     (_/    \n\n" ++
                        "Hasventory 0.0.1                            \n\n" ++
                        "Please enter the item ID or type 'menu':    ")
            
            input <- getLine
                        
            if (switchToMenuMode input) then 
                showMenu
            else
                doRentalAction input

            putStrLn "\n\nPress enter to start a new session..."
            _ <- getLine

            main


-- | the switch
switchToMenuMode :: String -> Bool
switchToMenuMode "menu" = True 
switchToMenuMode _      = False


doRentalAction :: String -> IO ()
doRentalAction itemID = do
    eitherItem <- getEitherItem itemID
    case eitherItem of
        Left err    -> print $ err
        Right item  -> doRentalAction' (fromJust item) itemID


doRentalAction' :: Item -> String -> IO ()
doRentalAction' item itemID = do
    case (latestRentalAction item) of
        Nothing                 -> borrowItem itemID >>= postNewRentalAction
        Just latestRentalAction -> doRentalAction'' itemID latestRentalAction


doRentalAction'' :: String -> Integer -> IO ()
doRentalAction'' itemID latestRentalActionID = do
    eitherRentalAction <- getEitherRentalAction latestRentalActionID
    case eitherRentalAction of
        Left err            -> print $ err
        Right rentalAction  -> doRentalAction''' itemID (fromJust rentalAction)


doRentalAction''' :: String -> RentalAction -> IO ()
doRentalAction''' itemID rentalAction = do
    if(lent rentalAction) then
        returnItem itemID rentalAction >>= returnItem'
    else
        borrowItem itemID >>= postNewRentalAction


returnItem' :: RentalAction -> IO ()
returnItem' rentalAction = do
    if(lent rentalAction) then
        putStrLn "Nothing done."
    else
        postNewRentalAction rentalAction


getEitherItem :: String -> IO (Either String (Maybe Item))
getEitherItem itemID = do
    let getItemURL = "http://localhost:3000/api/item/" ++ itemID
    response <- withManager $ httpLbs (applyBasicAuth user pass $ fromJust $ parseUrl getItemURL)
    return $ Right (decode $ responseBody response)
    where
        user = B8.pack "crt"
        pass = B8.pack "crt"


getEitherRentalAction :: Integer -> IO (Either String (Maybe RentalAction))
getEitherRentalAction rentalActionId = do
    let getRentalActionURL = "http://localhost:3000/api/rentalaction/" ++ (show rentalActionId)
    response <- withManager $ httpLbs (applyBasicAuth user pass $ fromJust $ parseUrl getRentalActionURL)
    return $ Right (decode $ responseBody response)
    where
        user = B8.pack "crt"
        pass = B8.pack "crt"


postNewRentalAction :: RentalAction -> IO ()
postNewRentalAction rentalAction = do
    runResourceT $ do
        manager <- liftIO $ newManager conduitManagerSettings
        initReq <- liftIO $ parseUrl "http://localhost:3000/api/post/rentalaction/"
        let req = initReq { method = "POST", requestBody = (RequestBodyLBS $ encode rentalAction) }
        res <- httpLbs (applyBasicAuth user pass req) manager
        return ()
        where
            user = B8.pack "crt"
            pass = B8.pack "crt"


borrowItem :: String -> IO RentalAction
borrowItem itemID  = do
    putStrLn "\nHow many days will this item be borrowed?"
    daysToBorrow <- getLine
    time <- {-liftIO-} getCurrentTime
    putStrLn "\nEnter the ID of the borrower. If the borrower is new, type his name."
    name <- getLine
    borrowerExists <- checkIfBorrowerExists name
    if(borrowerExists) then do
        borrowItem' itemID name daysToBorrow
    else do
        putStrLn "\nEnter the e-mail address auf the new borrower:"
        email <- getLine
        borrowerID <- postNewBorrower name email
        return $ RentalAction  [(read itemID)] True borrowerID time (Just $ addUTCTime ((realToFrac $ read daysToBorrow) * 84400) time) Nothing Nothing

-- TODO Wenn borrower ID nicht existiert
borrowItem' :: String -> String -> String -> IO RentalAction
borrowItem' itemID borrowerID daysToBorrow = do
    time <- liftIO getCurrentTime
    eitherBorrower <- getEitherBorrower borrowerID
    case eitherBorrower of
        Left err        -> return $ RentalAction [(read itemID)] True 0 time (Just $ addUTCTime ((realToFrac $ read daysToBorrow) * 84400) time) Nothing Nothing
        Right borrower  -> return $ RentalAction [(read itemID)] True (read borrowerID) time (Just $ addUTCTime ((realToFrac $ read daysToBorrow) * 84400) time) Nothing Nothing


postNewBorrower :: String -> String -> IO Integer
postNewBorrower name email = do
    runResourceT $ do
        manager <- liftIO $ newManager conduitManagerSettings
        initReq <- liftIO $ parseUrl "http://localhost:3000/api/post/borrower/"
        let req = initReq { method = "POST", requestBody = (RequestBodyLBS $ encode (Borrower name email Nothing False)) }
        res <- httpLbs (applyBasicAuth user pass req)  manager
        return $ read $ read $ show (responseBody res)
        where
            user = B8.pack "crt"
            pass = B8.pack "crt"


getEitherBorrower :: String -> IO (Either String (Maybe Borrower))
getEitherBorrower borrowerID = do
    let getBorrowerURL = "http://localhost:3000/api/borrower/" ++ borrowerID
    response <- withManager $ httpLbs (applyBasicAuth user pass $ fromJust $ parseUrl getBorrowerURL)
    return $ Right (decode $ responseBody response)
    where
        user = B8.pack "crt"
        pass = B8.pack "crt"


checkIfBorrowerExists :: String -> IO Bool
checkIfBorrowerExists borrowerID = do
    curl <- initialize
    let checkURL = "http://crt:crt@localhost:3000/api/borrower/status/" ++ borrowerID
    r <- do_curl curl checkURL []

    if((respBody r) == "EXISTS") then
        return True
    else
        return False


returnItem :: String -> RentalAction -> IO RentalAction
returnItem itemID rentalAction = do
    putStrLn $ "\nDo you want to return this item? Enter \"yes\" or \"no\"."
    response <- getLine
    time <- liftIO getCurrentTime
    if(response == "yes") then
        return $ RentalAction [(read itemID)] False (borrower rentalAction) time Nothing Nothing Nothing
    else
        return $ RentalAction [(read itemID)] True 0 time Nothing Nothing Nothing


showMenu :: IO ()
showMenu = do
    putStrLn (  "\nMENU:\n\n" ++
                "5 - Reboot\n" ++
                "6 - Exit" ++
                "\n\nPlease select one of the above options and press enter.")
    input <- getLine
    doMenuSelection input


doMenuSelection :: String -> IO ()
doMenuSelection "5" = rebootMachine 
doMenuSelection "6" = exitSuccess
doMenuSelection _   = putStrLn "This is not a valid selection."


rebootMachine :: IO ()
rebootMachine = do
     err <- system "sudo /sbin/reboot"
     case err of
         ExitFailure n  -> putStrLn $ "Error code: " ++ show n
         exitSuccess    -> return ()