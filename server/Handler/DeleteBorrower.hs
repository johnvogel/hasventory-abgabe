module Handler.DeleteBorrower where

import Import
import Database.Persist.Sqlite


-- GET Handler that either show a form to delete a borrower or a message
-- that the borrower cannot be deleted
getDeleteBorrowerR :: String -> Handler Html
getDeleteBorrowerR borrowerID = do
    entityItems           <- runDB $ selectList [] []
    entityRentalActions   <- runDB $ selectList [] []
    borrower <- runDB $ get404 $ BorrowerKey $ SqlBackendKey $ abs (read borrowerID)
    if(iterateItems False (map entityVal entityItems) (map entityVal entityRentalActions) (BorrowerKey $ SqlBackendKey $ abs (read borrowerID))) then
        cannotDeleteBorrower
        else
            getDeleteBorrowerR' borrowerID borrower


-- Continuation of "getDeleteBorrowerR"
getDeleteBorrowerR' :: String -> Borrower -> Handler Html
getDeleteBorrowerR' borrowerID borrower = 
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Archive Borrower
            <a href="/borrowers" class="options-link">&#8592; back
            <form method="post" action="/archive/borrower/#{borrowerID}">
                <p>Do you want to archive "#{borrowerName borrower}"?<br /><br />
                <button>archive
        |]


-- Iterates through all items and returns a Bool indicating, if one or more items
-- have been borrowed.
iterateItems :: Bool -> [Item] -> [RentalAction] -> Key Borrower -> Bool
iterateItems True _ _ _       = True
iterateItems False (x:xs) y z = iterateItems (checkIfItemBorowedByBorrower x y z) xs y z
iterateItems _ _ _ _          = False


-- Checks, if the current items has been borrowed by the borrower
checkIfItemBorowedByBorrower :: Item -> [RentalAction] -> Key Borrower -> Bool
checkIfItemBorowedByBorrower item rentalActions borrowerKey = do
    case (itemLatestRentalAction item) of
        Nothing                    -> False
        Just latestRentalActionKey -> checkIfItemBorowedByBorrower' (getRentalAction rentalActions latestRentalActionKey) borrowerKey

-- Continuation of checkIfItemBorowedByBorrower
checkIfItemBorowedByBorrower' :: RentalAction -> Key Borrower -> Bool
checkIfItemBorowedByBorrower' rentalAction borrowerKey = do
    (checkIfItemBorowedByBorrower'' (rentalActionBorrower rentalAction) borrowerKey) && (rentalActionLent rentalAction)


-- Continuation of checkIfItemBorowedByBorrower
checkIfItemBorowedByBorrower'' :: Key Borrower -> Key Borrower -> Bool
checkIfItemBorowedByBorrower'' x y | x == y    = True
                                   | otherwise = False
    

-- Takes a list of RentalActions and a key and return the RentalAction belonging
-- to the key.
getRentalAction :: [RentalAction] -> Key RentalAction -> RentalAction
getRentalAction rentalActions rentalActionKey =
    rentalActions!!((fromIntegral $ unSqlBackendKey $ toBackendKey $ rentalActionKey) - 1)


-- Hamlet code to show an error message
cannotDeleteBorrower :: Handler Html
cannotDeleteBorrower = 
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Archive Borrower
            <a href="/borrowers" class="options-link">&#8592; back
            <pre class="failure">This borrower cannot be archived, because he hasn't returned all his borrowed items.
        |]


-- POST handler that deletes the borrower.
postDeleteBorrowerR :: String -> Handler Html
postDeleteBorrowerR borrowerID = do
    runDB $ update (BorrowerKey $ SqlBackendKey $ abs (read borrowerID)) [BorrowerBorrowerArchived =. True]
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Archive Borrower
            <a href="/borrowers" class="options-link">&#8592; back
            <pre class="success">Borrower archived!
        |]
