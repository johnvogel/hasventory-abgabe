module Handler.BorrowerDetails where

import Import
import Data.Maybe
import Data.Time.Clock
import Data.Time.Format
import Database.Persist.Sqlite
import System.Locale


-- GET Handler that shows detailed information for a borrower. It displays all borrower
-- related information like name/email but also borrowed items and his rental history.
getBorrowerDetailsR :: String -> Handler Html
getBorrowerDetailsR borrowerID = do
    borrower            <- runDB $ get404 $ BorrowerKey $ SqlBackendKey $ abs (read borrowerID)
    entityItems         <- runDB $ selectList [] []
    entityRentalActions <- runDB $ selectList 
        [RentalActionBorrower ==. (BorrowerKey $ SqlBackendKey $ abs (read borrowerID))]
        [Desc RentalActionId]
    entityBorrowedRentalActions <- runDB $ selectList 
        [RentalActionBorrower ==. (BorrowerKey $ SqlBackendKey $ abs (read borrowerID))
        ,RentalActionLent ==. True]
        [Desc RentalActionId]

    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Borrower details
            <a href="/borrowers" class="options-link">&#8592; back
            <table>
                <tr>
                    <td>Name
                    <td>#{borrowerName borrower}
                <tr>
                    <td>E-Mail
                    <td>#{borrowerEmail borrower}
                $maybe comment <- (borrowerBorrowerComment borrower)
                    <tr>
                        <td>Comment
                        <td>#{unTextarea comment}
            <br />
            <h2>Currently borrowed items
            <table>
                <tr>
                    <th>Item ID
                    <th>Item
                    <th>Date
                    <th>Planned Return
                    <th>Comment
                $forall (Entity itemKey item) <- (lentItems entityItems entityBorrowedRentalActions)
                    <tr>
                        <td>#{show $ unSqlBackendKey $ toBackendKey itemKey}
                        <td>#{itemManufacturer item} #{itemModel item}
                        <td>#{formatTime defaultTimeLocale "%d.%m.%Y" $ rentalActionActionDate $ getRentalAction item entityBorrowedRentalActions}
                        <td>#{maybe "-" formatReturnDate (rentalActionPlannedReturnDate $ getRentalAction item entityBorrowedRentalActions)}
                        <td>#{maybe "-" unTextarea (rentalActionRentalComment $ getRentalAction item entityBorrowedRentalActions)}
            <br />
            <h2>Rental history
            <table>
                <tr>
                    <th>Rental ID
                    <th>Action
                    <th>Item(s)
                    <th>Date
                    <th>Planned Return
                    <th>Comment
                $forall (Entity rentalActionKey rentalAction) <- entityRentalActions
                    <tr>
                        <td>#{show $ unSqlBackendKey $ toBackendKey rentalActionKey}
                        <td>#{rentalType $ rentalActionLent rentalAction}
                        <td>#{show $ (map unSqlBackendKey (map toBackendKey (rentalActionItems rentalAction)))}
                        <td>#{formatTime defaultTimeLocale "%d.%m.%Y" $ rentalActionActionDate rentalAction}
                        <td>#{maybe "-" formatReturnDate (rentalActionPlannedReturnDate rentalAction)}
                        <td>#{maybe "-" unTextarea (rentalActionRentalComment rentalAction)}
        |]


-- Returns a Text that describes if the rentalAction was to borrow or return an item.
rentalType :: Bool -> Text
rentalType True  = "lent"
rentalType False = "returned"


-- Returns a list of all items borrowed by the borrower.
lentItems :: [Entity Item] -> [Entity RentalAction] -> [Entity Item]
lentItems _ []              = []
lentItems [] _              = []
lentItems [x] ys
    | isLentByBorrower x ys = x:lentItems [] ys
    | otherwise             =   lentItems [] ys
lentItems(x:xs) ys
    | isLentByBorrower x ys = x:lentItems xs ys
    | otherwise             =   lentItems xs ys


-- Checks, if the current item has been lent by the borrower.
isLentByBorrower :: Entity Item -> [Entity RentalAction] -> Bool
isLentByBorrower entityItem entityRentalActions = 
    isLentByBorrower' (entityVal entityItem) (map entityKey entityRentalActions)


-- Continuation of "isLentByBorrower"
isLentByBorrower' :: Item -> [Key RentalAction] -> Bool
isLentByBorrower' item rentalActionKeys = 
    case (itemLatestRentalAction item) of
        Nothing -> False
        Just x  -> (x `elem` rentalActionKeys)


-- Checks, if the RentalAction is the latest RentalAction of any item
isLatestRentalAction :: Entity RentalAction -> [Entity Item] -> Bool
isLatestRentalAction _ []                           = False
isLatestRentalAction (Entity x _) [(Entity _ y)]    = (maybe False ((==) x) (itemLatestRentalAction y))
isLatestRentalAction (Entity x z) ((Entity _ y):ys) = (maybe False ((==) x) (itemLatestRentalAction y)) || isLatestRentalAction (Entity x z) ys


-- Returns the latestRentalAction of an item
getRentalAction :: Item -> [Entity RentalAction] -> RentalAction
getRentalAction item entityRentalActions =
    getRentalAction' item (zip (map entityKey entityRentalActions) (map entityVal entityRentalActions))


-- Continuation of getRentalAction
getRentalAction' :: Item -> [(Key RentalAction, RentalAction)] -> RentalAction
getRentalAction' item rentalActionTupels =
    fromJust $ lookup (fromJust $ itemLatestRentalAction item) rentalActionTupels


-- Formats the plannedReturnDate
formatReturnDate :: UTCTime -> String
formatReturnDate dateTime =
    formatTime defaultTimeLocale "%d.%m.%Y" dateTime