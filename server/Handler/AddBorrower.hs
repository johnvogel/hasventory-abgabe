module Handler.AddBorrower where

import Import


-- GET Handler to display "Add Borrower" Form
getAddBorrowerR :: Handler Html
getAddBorrowerR = do
    (formWidget, formEnctype) <- generateFormPost addBorrowerForm
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Add Borrower
            <a href="/borrowers" class="options-link">&#8592; back
            <form method=post action=@{AddBorrowerR} enctype=#{formEnctype}>
                ^{formWidget}
                <button>Add Borrower
        |]


-- POST Handler to receive the data sent by the form
postAddBorrowerR :: Handler Html
postAddBorrowerR = do
    ((result, widget), enctype) <- runFormPost addBorrowerForm
    case result of
        FormSuccess borrower -> do
            _ <- addBorrowerToDatabase borrower
            return ()
        _ -> return ()

    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Add Borrower
            <a href="/borrowers" class="options-link">&#8592; back
            <pre class="success">Borrower added!
            <form method=post action=@{AddBorrowerR} enctype=#{enctype}>
                ^{widget}
                <button>Add Borrower
        |]


-- Applicative Form to create a Borrower
addBorrowerForm :: Form Borrower
addBorrowerForm = renderDivs $ Borrower
    <$> areq textField "Name*:" Nothing
    <*> areq emailField "E-Mail*:" Nothing
    <*> aopt textareaField "Comment:" Nothing
    <*> pure False


-- Function to add borrower to database
addBorrowerToDatabase :: Borrower -> Handler (Key Borrower)
addBorrowerToDatabase borrower = runDB $ do
    borrowerID <- insert borrower
    return borrowerID
