module Handler.GetInventoryRunThrough where

import Import
import System.IO.Unsafe
import Data.Time.Clock
import Database.Persist.Sqlite


-- API endpoint that creates a nbew inventoryRunThrough
getGetInventoryRunThroughR :: Handler Value
getGetInventoryRunThroughR = do
    runThroughID <- addRunThroughToDatabase (InventoryRunThrough (unsafePerformIO getCurrentTime))
    sendResponseStatus status200 ((show $ unSqlBackendKey $ toBackendKey runThroughID) :: String)


-- Adds an InventoryRunThrough to the database
addRunThroughToDatabase :: InventoryRunThrough -> Handler (Key InventoryRunThrough)
addRunThroughToDatabase runThrough = runDB $ do
    runThroughID <- insert runThrough
    return runThroughID