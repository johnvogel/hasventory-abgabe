module Handler.UnArchiveItem where

import Import
import Database.Persist.Sqlite


-- GET Handler to show a form to unarchive an item
getUnArchiveItemR :: String -> Handler Html
getUnArchiveItemR itemID = do
    item <- runDB $ get404 $ ItemKey $ SqlBackendKey $ abs (read itemID)
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Unarchive Item
            <a href="/archive" class="options-link">&#8592; back
            <form method="post" action="/unarchive/item/#{itemID}">
                Do you want to unarchive "#{itemManufacturer item} #{itemModel item}" (ID: #{itemID})?<br /><br />
                <button>unarchive
        |]


-- POST Handler that unarchives the item by updating the attribute
-- of the item in the database
postUnArchiveItemR :: String -> Handler Html
postUnArchiveItemR itemID = do
    runDB $ update (ItemKey $ SqlBackendKey $ abs (read itemID)) [ItemItemArchived =. False]
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Unarchive Item
            <a href="/archive" class="options-link">&#8592; back
            <pre class="success">Item unarchived!
        |]
