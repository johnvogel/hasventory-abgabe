module Handler.ExtendRental where

import Import
import Data.Time
import Data.Text (pack, unpack)
import Database.Persist.Sqlite
import System.IO.Unsafe
import System.Locale (defaultTimeLocale)
import qualified Data.Time.Format (parseTime)


-- GET Handler, that checks if the items has been archived. If not it
-- displays the extendRentalForm
getExtendRentalR :: String -> Handler Html
getExtendRentalR itemID = do
    item <- runDB $ get404 $ ItemKey $ SqlBackendKey $ abs (read itemID)
    if(itemItemArchived item) then
        errorItemArchived
        else
            case (itemLatestRentalAction item) of
                Nothing              -> errorNotLent
                Just rentalActionKey -> getExtendRentalR' itemID rentalActionKey


-- Continuation of getExtendRentalR
getExtendRentalR' :: String -> Key RentalAction -> Handler Html
getExtendRentalR' itemID rentalActionKey = do
    rentalAction <- runDB $ get404 rentalActionKey
    if(rentalActionLent rentalAction) then
        showExtendRentalForm itemID rentalAction
        else
            errorNotLent


-- Display the ExtendRentalForm
showExtendRentalForm :: String -> RentalAction -> Handler Html
showExtendRentalForm itemID rentalAction = do
    (formWidget, formEnctype) <- generateFormPost (extendRentalForm itemID rentalAction)
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Extend Rental
            <a href="/items/" class="options-link">&#8592; back
            <form method=post action="/extend/item/#{itemID}" enctype=#{formEnctype}>
                ^{formWidget}
                <button>Extend
        |]


-- Form for creating a new rentalAction. Only the plannedReturnDate and
-- the comment can be changed, everything else will be copied from the last
-- RentalAction
extendRentalForm :: String -> RentalAction -> Form RentalAction
extendRentalForm itemID rentalAction = renderDivs $ RentalAction
    <$> pure [(ItemKey $ SqlBackendKey $ abs (read itemID))]
    <*> pure True
    <*> pure (rentalActionBorrower rentalAction)
    <*> pure (unsafePerformIO getCurrentTime)
    <*> aopt utcTimeField "New return date* (dd.mm.yyyy):" (Just $ Just $ addUTCTime 86400 (unsafePerformIO getCurrentTime))
    <*> aopt textareaField "Comment:" Nothing
    <*> pure (rentalActionPermanentRental rentalAction)

    where
        utcTimeField :: Monad m => RenderMessage (HandlerSite m) FormMessage => Field m UTCTime
        utcTimeField = Field
             { fieldParse = parseHelper parseTime'
             , fieldView = \theId name attrs val isReq -> toWidget [hamlet|
        $newline never
        <input id="#{theId}" name="#{name}" type="datetime" *{attrs} :isReq:required="" value="#{showVal val}">
        |]
             , fieldEnctype = UrlEncoded
             }
            where
               showVal = either id (pack . formatTime defaultTimeLocale "%d.%m.%Y")

        parseTime' :: Text -> Either FormMessage UTCTime
        parseTime' theText = 
            maybe 
               (Left MsgInvalidTimeFormat) 
               (\x -> Right x) 
               (Data.Time.Format.parseTime defaultTimeLocale "%d.%m.%Y" $ unpack theText)


-- Error message if the item has been archived already
errorItemArchived :: Handler Html
errorItemArchived = defaultLayout $ do
    setTitle "Hasventory"
    [whamlet|
        <h2>Extend Rental
        <a href="/items/" class="options-link">&#8592; back
        <pre class="failure">Error: This item is archived!
        |]


-- Error message if the item is currently not lent
errorNotLent :: Handler Html
errorNotLent = defaultLayout $ do
    setTitle "Hasventory"
    [whamlet|
        <h2>Extend Rental
        <a href="/items/" class="options-link">&#8592; back
        <pre class="failure">Error: This item is currently not lent to someone!
        |]


-- POST Handler that receives the data from the extendRentalForm
postExtendRentalR :: String -> Handler Html
postExtendRentalR itemID = do
    item <- runDB $ get404 $ ItemKey $ SqlBackendKey $ abs (read itemID)
    case (itemLatestRentalAction item) of
        Nothing              -> formFailure
        Just rentalActionKey -> postExtendRentalR' itemID rentalActionKey


-- Continuation of postExtendRentalR
postExtendRentalR' :: String -> Key RentalAction -> Handler Html
postExtendRentalR' itemID rentalActionKey = do
    oldRentalAction <- runDB $ get404 rentalActionKey
    ((result, _), _) <- runFormPost (extendRentalForm itemID oldRentalAction)
    case result of
        FormSuccess rentalAction ->  do 
                                        _ <- addRentalActionToDatabase rentalAction
                                        formSuccess
        FormFailure _            ->  do
                                        formFailure
        _                        ->  do
                                        formFailure


-- Success message
formSuccess :: Handler Html
formSuccess = do
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Extend Rental
            <a href="/items/" class="options-link">&#8592; back
            <pre class="success">Rental extended!
        |]


-- Error message
formFailure :: Handler Html
formFailure = do
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Extend Rental
            <a href="/items/" class="options-link">&#8592; back
            <pre class="failure">Error: Rental not extended!
        |]


-- Function to add RentalAction to the database and update the reference to
-- the latestRentalAction in the item
addRentalActionToDatabase :: RentalAction -> Handler (Key RentalAction)
addRentalActionToDatabase rentalAction = runDB $ do
    rentalActionKey <- insert rentalAction
    updateWhere [ItemId <-. (rentalActionItems rentalAction)] [ItemLatestRentalAction =. (Just rentalActionKey)]
    return rentalActionKey