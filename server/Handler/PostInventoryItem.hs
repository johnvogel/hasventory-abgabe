module Handler.PostInventoryItem where

import Import
import qualified Data.Aeson as J
import Database.Persist.Sqlite


-- API Endpoint to add n item, returns the ID of the new item
postPostInventoryItemR :: Handler Value
postPostInventoryItemR = do
    result <- parseJsonBody :: Handler (J.Result InventoryItem)
    case result of
        J.Error _                -> sendResponseStatus status400 ("JSON NOT VALID" :: String)
        J.Success inventoryItem  -> postPostInventoryItemR' inventoryItem


-- Continuation of postPostInventoryItemR
postPostInventoryItemR' :: InventoryItem -> Handler Value
postPostInventoryItemR' inventoryItem = do
    inventoryItemID <- addInventoryItemToDatabase inventoryItem
    sendResponseStatus status200 ((show $ unSqlBackendKey $ toBackendKey inventoryItemID):: String)


-- Adds the item to the database
addInventoryItemToDatabase :: InventoryItem -> Handler (Key InventoryItem)
addInventoryItemToDatabase inventoryItem = runDB $ do
    inventoryItemID <- insert inventoryItem
    return inventoryItemID