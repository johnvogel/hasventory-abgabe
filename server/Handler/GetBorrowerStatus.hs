module Handler.GetBorrowerStatus where

import Import
import Database.Persist.Sqlite


-- API endpoint that returns "EXISTS" if the borrower ID is valid
getGetBorrowerStatusR :: String -> Handler ()
getGetBorrowerStatusR borrowerID = do
    maybeBorrower <- runDB $ get $ BorrowerKey $ SqlBackendKey $ abs (read borrowerID)
    case maybeBorrower of
        Nothing 		-> sendResponseStatus status404 ("ID NOT VALID" :: Text)
        Just _ 	-> sendResponseStatus status200 ("EXISTS" :: Text)
