module Handler.DeleteItem where

import Import
import Database.Persist.Sqlite


-- GET Handlet that either show a form to archive (delete) an item or 
-- an error message stating that it is not possible to archive this item
getDeleteItemR :: String -> Handler Html
getDeleteItemR itemID = do
    item <- runDB $ get404 $ ItemKey $ SqlBackendKey $ abs (read itemID)
    case (itemLatestRentalAction item) of
        Nothing              -> showArchiveForm itemID item
        Just rentalActionKey -> getDeleteItemR' itemID item rentalActionKey


-- Continuation of getDeleteItemR
getDeleteItemR' :: String -> Item -> Key RentalAction -> Handler Html
getDeleteItemR' itemID item rentalActionKey = do
    rentalAction <- runDB $ get404 rentalActionKey
    if(rentalActionLent rentalAction) then
        cannotArchiveItem
        else
            showArchiveForm itemID item


-- Shows the form to archive (delete) an item
showArchiveForm :: String -> Item -> Handler Html
showArchiveForm itemID item = do
    defaultLayout $ do
            setTitle "Hasventory"
            [whamlet|
                <h2>Archive Item
                <a href="/items/" class="options-link">&#8592; back
                <form method="post" action="/archive/item/#{itemID}">
                    Do you want to archive "#{itemManufacturer item} #{itemModel item}" (ID: #{itemID})?<br /><br />
                    <button>archive
            |]


-- Error message, which is shown, if the item is still borrowed
cannotArchiveItem :: Handler Html
cannotArchiveItem = do
    defaultLayout $ do
            setTitle "Hasventory"
            [whamlet|
                <h2>Archive Item
                <a href="/items/" class="options-link">&#8592; back
                <pre class="failure">This item cannot be archived, as it is still borrowed.
            |]


-- POST Handler that actually archives (deletes) the item
postDeleteItemR :: String -> Handler Html
postDeleteItemR itemID = do
    runDB $ update (ItemKey $ SqlBackendKey $ abs (read itemID)) [ItemItemArchived =. True]
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Archive Item
            <a href="/items/" class="options-link">&#8592; back
            <pre class="success">Item archived!
        |]