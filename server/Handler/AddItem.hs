module Handler.AddItem where

import Import


-- GET Handler to display Add Item Forum
getAddItemR :: Handler Html
getAddItemR = do
    (widget, enctype) <- generateFormPost addItemForm
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Add Item
            <a href="/items/" class="options-link">&#8592; back
            <form method=post action=@{AddItemR} enctype=#{enctype}>
                ^{widget}
                <button>Add Item
        |]


-- POST Handler to receive the data sent by the form
postAddItemR :: Handler Html
postAddItemR = do
    ((result, widget), enctype) <- runFormPost addItemForm
    case result of
        FormSuccess item    ->  do 
                                    _ <- addItemToDatabase item
                                    return ()
        _                   ->  return ()

    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Add Item
            <a href="/items/" class="options-link">&#8592; back
            <pre class="success">Item added!
            <form method=post action=@{AddItemR} enctype=#{enctype}>
                ^{widget}
                <button>Add Item
        |]


-- Applicative Form to create an item
addItemForm :: Form Item
addItemForm = renderDivs $ Item
    <$> areq textField "Manufacturer*:" Nothing
    <*> areq textField "Model*:" Nothing
    <*> areq (selectFieldList types) "Type*:" Nothing
    <*> aopt textField "IMEI/Serial number:" Nothing
    <*> aopt textField "Old Item ID:" Nothing
    <*> aopt textareaField "Comment:" Nothing
    <*> areq checkBoxField "Marketing device:" Nothing
    <*> pure Nothing
    <*> pure False
    where
        types :: [(Text, Text)]
        types = [("Smartphone", "Smartphone")
                , ("Featurephone", "Featurephone")
                , ("Tablet", "Tablet")
                , ("Surfstick", "Surfstick")
                , ("Laptop", "Laptop")
                , ("Zubehör", "Zubehör")
                , ("Sonstiges", "Sonstiges")]


-- Function to add Item to database
addItemToDatabase :: Item -> Handler (Key Item)
addItemToDatabase item = runDB $ do
    itemID <- insert item
    return itemID