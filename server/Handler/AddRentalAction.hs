module Handler.AddRentalAction where

import Import
import Data.List (head)
import Data.Time.Clock
import System.IO.Unsafe
import Database.Persist.Sqlite
import Data.Time
import Data.Text (pack, unpack)
import System.Locale (defaultTimeLocale)
import qualified Data.Time.Format (parseTime)


-- GET Handler, that checks if the items has been archived or if the item
-- should be borrowed or returned
getAddRentalActionR :: String -> Handler Html
getAddRentalActionR itemID = do
    item <- runDB $ get404 $ ItemKey $ SqlBackendKey $ abs (read itemID)
    if(itemItemArchived item) then
        cannotAddRentalAction
        else
            case (itemLatestRentalAction item) of
                Nothing              -> borrowItem itemID item
                Just rentalActionKey -> getAddRentalActionR' itemID item rentalActionKey


-- Continuation of the getAddRentalActionR Function
getAddRentalActionR' :: String -> Item -> Key RentalAction -> Handler Html
getAddRentalActionR' itemID item rentalActionKey = do
    rentalAction <- runDB $ get404 rentalActionKey
    if(rentalActionLent rentalAction) then
        returnItem itemID item rentalAction
        else
            borrowItem itemID item


-- Displays the forms to return an item or to extend the rental. Both forms will
-- be directed to different endpoints.
returnItem :: String -> Item -> RentalAction -> Handler Html
returnItem itemID item rentalAction = do
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Manage item
            <a href="/items/" class="options-link">&#8592; back
            <br />
            Please choose between one of the two options:
            <br /><br />
            <div class="return-box">
                <h3>Return item
                Do you want to return "#{itemManufacturer item} #{itemModel item}" (ID: #{itemID})?<br /><br />
                <form method="post" action="/return/item/#{itemID}/#{show $ unSqlBackendKey $ toBackendKey (rentalActionBorrower rentalAction)}">
                    <button>return
            <div class="extend-box">
                <h3>Extend rental
                Do you want to extend the rental of "#{itemManufacturer item} #{itemModel item}" (ID: #{itemID})?<br /><br />
                <form method="get" action="/extend/item/#{itemID}">
                    <button>extend
            <br class="break">
        |]


-- Displays the form to lend an item.
borrowItem :: String -> Item -> Handler Html
borrowItem itemID item = do
    borrowers <- runDB $ selectList [BorrowerBorrowerArchived ==. False] []
    (formWidget, _) <- generateFormPost (borrowItemForm itemID borrowers)
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Lend item
            <a href="/items/" class="options-link">&#8592; back
            <form method="post" action="/add/rentalaction/#{itemID}">
                Do you want to lend "#{itemManufacturer item} #{itemModel item}" (ID: #{itemID})?<br /><br />
                ^{formWidget}
                <br /><button>lend
        |]


-- Applicative form to lend one item
borrowItemForm :: String -> [Entity Borrower] -> Form RentalAction
borrowItemForm itemID borrowers = renderDivs $ RentalAction
    <$> pure [(ItemKey $ SqlBackendKey $ abs (read itemID))]
    <*> pure True
    <*> areq (selectFieldList $ borrowerList borrowers) "Borrower*:" (Just $ entityKey $ head borrowers)
    <*> pure (unsafePerformIO getCurrentTime)
    <*> aopt utcTimeField "Return date* (dd.mm.yyyy):" (Just $ Just $ addUTCTime 86400 (unsafePerformIO getCurrentTime))
    <*> aopt textareaField "Comment:" Nothing
    <*> aopt checkBoxField "Permanent Rental:" Nothing

    where
        -- Creates a list of tuples consisting of the name and the key of the borrower
        borrowerList :: [Entity Borrower] -> [(Text, Key Borrower)]
        borrowerList entityBorrowers = do
            map (\borrower -> (borrowerName $ entityVal borrower, entityKey borrower)) entityBorrowers

        -- Defines a custom field that accepts a date
        utcTimeField :: Monad m => RenderMessage (HandlerSite m) FormMessage => Field m UTCTime
        utcTimeField = Field
             { fieldParse = parseHelper parseTime'
             , fieldView = \theId name attrs val isReq -> toWidget [hamlet|
        $newline never
        <input id="#{theId}" name="#{name}" type="datetime" *{attrs} :isReq:required="" value="#{showVal val}">
        |]
             , fieldEnctype = UrlEncoded
             }
            where
               showVal = either id (pack . formatTime defaultTimeLocale "%d.%m.%Y")

        -- Parses the planedReturnDate toa UTCTime
        parseTime' :: Text -> Either FormMessage UTCTime
        parseTime' theText = 
            maybe 
               (Left MsgInvalidTimeFormat) 
               (\x -> Right x) 
               (Data.Time.Format.parseTime defaultTimeLocale "%d.%m.%Y" $ unpack theText)


-- Failure message
cannotAddRentalAction :: Handler Html
cannotAddRentalAction = defaultLayout $ do
    setTitle "Hasventory"
    [whamlet|
        <h2>Lend/Return item
        <a href="/items/" class="options-link">&#8592; back
        <pre class="failure">Error: This item is archived!
        |]


-- POST Handler that receives the data from the borrowItemForm
postAddRentalActionR :: String -> Handler Html
postAddRentalActionR itemID = do
    borrowers <- runDB $ selectList [] []
    ((result, _), _) <- runFormPost (borrowItemForm itemID borrowers)
    case result of
        FormSuccess rentalAction ->  do 
                                        _ <- addRentalActionToDatabase rentalAction
                                        formSuccess
        FormFailure _            ->  do
                                        formFailure
        _                        ->  do
                                        formFailure


-- Success message
formSuccess :: Handler Html
formSuccess = do
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Lend item
            <a href="/items/" class="options-link">&#8592; back
            <pre class="success">Item lent!
        |]


-- Failure message
formFailure :: Handler Html
formFailure = do
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Lend item
            <a href="/items/" class="options-link">&#8592; back
            <pre class="failure">Error: Item not lent!
        |]


-- Function to add RentalAction to the database and update the reference to
-- the latestRentalAction in the item
addRentalActionToDatabase :: RentalAction -> Handler (Key RentalAction)
addRentalActionToDatabase rentalAction = runDB $ do
    rentalActionKey <- insert rentalAction
    updateWhere [ItemId <-. (rentalActionItems rentalAction)] [ItemLatestRentalAction =. (Just rentalActionKey)]
    return rentalActionKey