module Handler.Home where

import Import

-- Shows the homepage with boxes for the different pages
getHomeR :: Handler Html
getHomeR = do
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Home
            <a href="/items/" class="home-box items-box">
                <h3>Items
                <p>On this page you can see a list of all items and their details.
                <ul>
                    <li><span onclick="document.location.href = '/items/'; return false">lent items</span>
                    <li><span onclick="document.location.href = '/add/item/'; return false">add item</span>
            <a href="/borrowers/" class="home-box borrowers-box">
                <h3>Borrowers
                <p>Here you can see a list of all registered borrowers.
                <ul>
                    <li><span onclick="document.location.href = '/borrowers/'; return false">manage borrowers</span>
                    <li><span onclick="document.location.href = '/add/borrower/'; return false">add borrower</span>
            <a href="/archive/" class="home-box archive-box">
                <h3>Archive
                <p>This page gives you a list of all archived items and borrowers.
            <div class="break">
        |]