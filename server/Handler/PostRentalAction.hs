module Handler.PostRentalAction where

import Import
import qualified Data.Aeson as J
import Database.Persist.Sqlite


-- API Endpoint to add a RentalAction, returns the ID of the new RentalAction
postPostRentalActionR :: Handler Value
postPostRentalActionR = do
    result <- parseJsonBody :: Handler (J.Result RentalAction)
    case result of
        J.Error _               -> sendResponseStatus status400 ("JSON NOT VALID" :: String)
        J.Success rentalAction  -> postPostRentalActionR' rentalAction


-- Continuation of postPostRentalActionR
postPostRentalActionR' :: RentalAction -> Handler Value
postPostRentalActionR' rentalAction = do
    rentalActionKey <- addRentalActionToDatabase rentalAction
    sendResponseStatus status200 ((show $ unSqlBackendKey $ toBackendKey rentalActionKey):: String)


-- Adds the RentalAction to the database
addRentalActionToDatabase :: RentalAction -> Handler (Key RentalAction)
addRentalActionToDatabase rentalAction = runDB $ do
    rentalActionKey <- insert rentalAction
    updateWhere [ItemId <-. (rentalActionItems rentalAction)] [ItemLatestRentalAction =. (Just rentalActionKey)]
    return rentalActionKey