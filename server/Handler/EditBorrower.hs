module Handler.EditBorrower where

import Import
import Database.Persist.Sqlite


-- GET Handler to show the editBorrower form.
getEditBorrowerR :: String -> Handler Html
getEditBorrowerR borrowerID = do
    borrower <- getBorrower borrowerID
    (formWidget, formEnctype) <- generateFormPost (editBorrowerForm borrower)
    if(borrowerBorrowerArchived borrower) then
        cannotEditBorrower
        else
            defaultLayout $ do
                setTitle "Hasventory"
                [whamlet|
                    <h2>Edit Borrower
                    <a href="/borrowers" class="options-link">&#8592; back
                    <form method=post action="/edit/borrower/#{borrowerID}" enctype=#{formEnctype}>
                        ^{formWidget}
                        <button>Edit Borrower
                |]


-- Applicative form to edit the borrower. Is prefilled with the data
-- from the selected borrower
editBorrowerForm :: Borrower -> Form Borrower
editBorrowerForm borrower = renderDivs $ Borrower
    <$> areq textField "Name*:" (Just $ borrowerName borrower)
    <*> areq textField "E-Mail*:" (Just $ borrowerEmail borrower)
    <*> aopt textareaField "Comment:" (Just $ borrowerBorrowerComment borrower)
    <*> pure (borrowerBorrowerArchived borrower)


-- Gets the borrower from the database
getBorrower :: String -> Handler (Borrower)
getBorrower borrowerID = do
    runDB $ get404 $ BorrowerKey $ SqlBackendKey $ abs (read borrowerID)


-- POST Handler that shows a succes or error message
postEditBorrowerR :: String -> Handler Html
postEditBorrowerR borrowerID = do
    borrower <- getBorrower borrowerID
    ((result, _), _) <- runFormPost (editBorrowerForm borrower)
    case result of
        FormSuccess fromBorrower    ->  do 
                                    _ <- updateBorrowerInDatabase borrowerID fromBorrower
                                    return ()
        _                       ->  return ()
    insertedBrrower <- getBorrower borrowerID
    (formWidget, formEnctype) <- generateFormPost (editBorrowerForm insertedBrrower)
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Edit Borrower
            <a href="/borrowers" class="options-link">&#8592; back
            <pre class="success">Edited Borrower saved!
            <form method=post action="/edit/borrower/#{borrowerID}" enctype=#{formEnctype}>
                ^{formWidget}
                <button>Edit Borrower
        |]


-- Updates the borrower by replacing the entity in the database
updateBorrowerInDatabase :: String -> Borrower -> Handler ()
updateBorrowerInDatabase borrowerID borrower = do
    runDB $ do
        replace (BorrowerKey $ SqlBackendKey $ abs (read borrowerID)) borrower
        return ()


-- Shows an error message that the borrower can't be deleted because he is archived.
cannotEditBorrower :: Handler Html
cannotEditBorrower = defaultLayout $ do
    setTitle "Hasventory"
    [whamlet|
        <h2>Edit Borrower
        <a href="/borrowers" class="options-link">&#8592; back
        <pre class="failure">Error: This borrower is archived!
    |]