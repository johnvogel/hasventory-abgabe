{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Handler.Items where

import Import
import Data.List
import Data.Maybe
import Data.Time
import Data.Text (append, pack, unpack)
import System.Locale (defaultTimeLocale)
import System.IO.Unsafe
import Database.Persist.Sqlite
import qualified Data.Time.Format (parseTime)


-- GET Handler to show a list of all non-archived items.
getItemsR :: Handler Html
getItemsR = do
    --maybeAction     <- lookupGetParam  "filter"
    items           <- runDB $ selectList [ItemItemArchived ==. False] []
    rentalActions   <- runDB $ selectList [] []
    borrowersHome   <- runDB $ selectList [] []
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Items
            <a href="/add/item" class="options-link">+ add item
            <form method="post">
                <table class="item-table">
                    <tr>
                        <th>
                        <th>ID
                        <th>Item
                        <th>Typ
                        <th>IMEI
                        <th>Status
                        <th>Comment
                        <th>Actions
                    $forall (Entity itemKey item) <- items
                        <tr>
                            <td><span><input type="checkbox" name="item" value="#{show $ unSqlBackendKey $ toBackendKey itemKey}" id="#{show $ unSqlBackendKey $ toBackendKey itemKey}" style="margin-bottom: 3px;" /></span>
                            <td><label for="#{show $ unSqlBackendKey $ toBackendKey itemKey}"><span class="item-id">#{show $ unSqlBackendKey $ toBackendKey itemKey}</span></label>
                            <td><label for="#{show $ unSqlBackendKey $ toBackendKey itemKey}">#{itemManufacturer item} #{itemModel item}</label>
                            <td><label for="#{show $ unSqlBackendKey $ toBackendKey itemKey}">#{itemItemType item}</label>
                            <td><label for="#{show $ unSqlBackendKey $ toBackendKey itemKey}">#{maybe "-" unpack (itemSerialNumber item)}</label>
                            <td><label for="#{show $ unSqlBackendKey $ toBackendKey itemKey}"><span class="#{showRentalStatus rentalActions item}">#{showRentalStatus rentalActions item}#{showBorrower rentalActions borrowersHome item}</span></label>
                            <td><label for="#{show $ unSqlBackendKey $ toBackendKey itemKey}">#{showComment rentalActions item}</label>
                            <td>
                                <ul class="horizontal-list table-row-options">
                                    <li><a href="/item/#{show $ unSqlBackendKey $ toBackendKey itemKey}"><img src="/static/img/info-icon.png" alt="details" title="details"></a>
                                    <li><a href="/edit/item/#{show $ unSqlBackendKey $ toBackendKey itemKey}"><img src="/static/img/pencil-icon.png" alt="edit" title="edit"></a>
                                    <li><a href="/archive/item/#{show $ unSqlBackendKey $ toBackendKey itemKey}"><img src="/static/img/trash-can-icon.png" alt="archive" title="archive"></a>
                                    <li><a href="/add/rentalaction/#{show $ unSqlBackendKey $ toBackendKey itemKey}"><img src="/static/img/arrows-icon.png" alt="lend/return/extend" title="lend/return/extend"></a>
                <ul class="horizontal-list bottom menu no-disc-list">
                    <li>&#x21b3; <input type="submit" value="lend" name="homeMultiAction" />
            <br class="break bottom-table"/>
        |]


-- POST Handler for multiple item rentals
postItemsR :: Handler Html
postItemsR = do
    maybeAction         <- lookupPostParam  "homeMultiAction"
    itemIDs             <- lookupPostParams "item"
    borrowersForm       <- runDB $ selectList [BorrowerBorrowerArchived ==. False] []
    (lendFormWidget, _) <- generateFormPost (lendItemForm (map unpack itemIDs) borrowersForm)
    itemsInStock        <- checkIfItemsInStock (map unpack itemIDs)

    defaultLayout $ do
        setTitle "Hasventory"
        case (itemsInStock, ((length itemIDs) > 0), maybeAction) of
            (True, True, Just "lend") ->
                [whamlet|
                    <h2>Lend #{length itemIDs} items
                    <a href="/items/" class="options-link">&#8592; back
                    <form method="post" action="/add/multirental">
                        <br />Do you want to lend the following item(s)?
                        <ul class="horizontal-list no-disc-list">
                            $forall itemID <- itemIDs
                                <li><input type="checkbox" name="item" id="#{unpack itemID}" value="#{unpack itemID}" checked /><label for="#{unpack itemID}">#{unpack itemID}</label>
                        <br class="break"/>
                        ^{lendFormWidget}<br />
                        <button>lend
                |]

            (_, False, _) ->
                [whamlet|
                    <h2>Borrow/Archive multiple items
                    <a href="/items/" class="options-link">&#8592; back
                    <pre class="failure">Error: Please select at least one item.
                |]

            (False, _, _) ->
                [whamlet|
                    <h2>Borrow/Archive multiple items
                    <a href="/items/" class="options-link">&#8592; back
                    <pre class="failure">Error: All selected items must be in stock.
                |]

            (_,_,_) ->
                [whamlet|
                    <h2>Borrow/Archive multiple items
                    <a href="/items/" class="options-link">&#8592; back
                    <pre class="failure">Error!
                |]


-- Gets a list of itemIDs and returns a bool that indicates if one or more items
-- are currently not in stock
checkIfItemsInStock :: [String] -> Handler (Bool)
checkIfItemsInStock itemIDs = do
    items         <- runDB $ mapM get404 (map ItemKey (map SqlBackendKey (map read itemIDs)))
    rentalActions <- runDB $ selectList [] []
    return (checkIfItemsInStock' items (map entityVal rentalActions))


-- Continuation of checkIfItemsInStock
checkIfItemsInStock' :: [Item] -> [RentalAction] -> Bool
checkIfItemsInStock' []     _ = False
checkIfItemsInStock' [x]    y = (maybe True (isInStock y) (itemLatestRentalAction x))
checkIfItemsInStock' (x:xs) y = (maybe True (isInStock y) (itemLatestRentalAction x)) && checkIfItemsInStock' xs y


-- Applicative Form to lend multiple items
lendItemForm :: [String] -> [Entity Borrower] -> Form RentalAction
lendItemForm itemIDs borrowers = renderDivs $ RentalAction
    <$> pure (map ItemKey (map SqlBackendKey (map read itemIDs)))
    <*> pure True
    <*> areq (selectFieldList $ borrowerList borrowers) "Borrower*:" (Just $ entityKey $ head borrowers)
    <*> pure (unsafePerformIO getCurrentTime)
    <*> aopt utcTimeField "Return date* (dd.mm.yyyy):" (Just $ Just $ addUTCTime 86400 (unsafePerformIO getCurrentTime))
    <*> aopt textareaField "Comment:" Nothing
    <*> aopt checkBoxField "Permanent Rental:" Nothing

    where
        -- Creates a list of tuples consisting of the name and the key of the borrower
        borrowerList :: [Entity Borrower] -> [(Text, Key Borrower)]
        borrowerList entityBorrowers = do
            map (\borrower -> (borrowerName $ entityVal borrower, entityKey borrower)) entityBorrowers

        -- Defines a custom field that accepts a date
        utcTimeField :: Monad m => RenderMessage (HandlerSite m) FormMessage => Field m UTCTime
        utcTimeField = Field
             { fieldParse = parseHelper parseTime'
             , fieldView = \theId name attrs val isReq -> toWidget [hamlet|
        $newline never
        <input id="#{theId}" name="#{name}" type="datetime" *{attrs} :isReq:required="" value="#{showVal val}">
        |]
             , fieldEnctype = UrlEncoded
             }
            where
               showVal = either id (pack . formatTime defaultTimeLocale "%d.%m.%Y")

        -- Parses the planedReturnDate toa UTCTime
        parseTime' :: Text -> Either FormMessage UTCTime
        parseTime' theText = 
            maybe 
               (Left MsgInvalidTimeFormat) 
               (\x -> Right x) 
               (Data.Time.Format.parseTime defaultTimeLocale "%d.%m.%Y" $ unpack theText)


-- Returns the rentalStatus if an item as Text
showRentalStatus :: [Entity RentalAction] -> Item -> Text
showRentalStatus entityRentalActions item = do
    latestRentalActionStatus
        (map entityVal entityRentalActions)
        (itemLatestRentalAction item)

-- Continuation of showRentalStatus
latestRentalActionStatus :: [RentalAction] -> Maybe (Key RentalAction) -> Text
latestRentalActionStatus rentalActions maybeRentalActionKey =
    case maybeRentalActionKey of
        Nothing              ->  "in stock"
        Just rentalActionKey ->  latestRentalActionStatus' rentalActions rentalActionKey

-- Continuation of showRentalStatus
latestRentalActionStatus' :: [RentalAction] -> Key RentalAction -> Text
latestRentalActionStatus' rentalActions rentalActionKey = do
    case (isLent rentalActions rentalActionKey) of
        True    ->  checkIfPermanent
                        (calcTimeDifference (returnTime rentalActions rentalActionKey) (unsafePerformIO getCurrentTime))
                        (getRentalAction rentalActions rentalActionKey)
        False   ->  "in stock"

-- Returns a text if the rental of an item is overdue
checkIfOverdue :: NominalDiffTime -> Text
checkIfOverdue x | x <  0    = "overdue"
                 | otherwise = "lent"


-- Returns a text to indicate, if an item has been lent permanently
checkIfPermanent :: NominalDiffTime -> RentalAction -> Text
checkIfPermanent x y | (isJust $ rentalActionPermanentRental y) = checkIfPermanent' (fromJust $ rentalActionPermanentRental y) x
                     | otherwise                                = checkIfOverdue x


-- Continuation of checkIfPermanent
checkIfPermanent' :: Bool -> NominalDiffTime -> Text
checkIfPermanent' permanent x | permanent = "permanent"
                              | otherwise = checkIfOverdue x


-- Calculates the time differences between to UTCTimes
calcTimeDifference :: UTCTime -> UTCTime -> NominalDiffTime
calcTimeDifference returnUTCTime now = do
    diffUTCTime returnUTCTime now


{-
calcTimeDifference :: UTCTime -> IO NominalDiffTime
calcTimeDifference returnUTCTime = do
    difference <- return . diffUTCTime returnUTCTime =<< getCurrentTime
    return difference
-}


-- Return the plannedReturnTime of a RentalAction
returnTime :: [RentalAction] -> Key RentalAction -> UTCTime
returnTime rentalActions rentalActionKey =
    fromJust $ rentalActionPlannedReturnDate $ getRentalAction rentalActions rentalActionKey


-- Returns a Bool that indicates, if an item is in stock
isInStock :: [RentalAction] -> Key RentalAction -> Bool
isInStock rentalActions rentalActionKey =
    not $ rentalActionLent $ getRentalAction rentalActions rentalActionKey


-- Returns a Bool that indicates, if an item is currently lent
isLent :: [RentalAction] -> Key RentalAction -> Bool
isLent rentalActions rentalActionKey =
    rentalActionLent $ getRentalAction rentalActions rentalActionKey


-- Gets a list of RentalActions and a RentalAction Key and returns the RentalAction
-- with that key.
getRentalAction :: [RentalAction] -> Key RentalAction -> RentalAction
getRentalAction rentalActions rentalActionKey =
    rentalActions!!((fromIntegral $ unSqlBackendKey $ toBackendKey $ rentalActionKey) - 1)


-- Shows the borrower of the latestRentalAction of a specified item
showBorrower :: [Entity RentalAction] -> [Entity Borrower] -> Item -> Text
showBorrower entityRentalActions entityBorrowers item = do
    borrowerColumnName
        (map entityVal entityRentalActions)
        (entityBorrowers)
        (itemLatestRentalAction item)


-- Returns the string to display in the column "Borrower"of the items table
borrowerColumnName :: [RentalAction] -> [Entity Borrower] -> Maybe (Key RentalAction) -> Text
borrowerColumnName rentalActions entityBorrowers maybeRentalActionKey =
    case (maybeRentalActionKey) of
        Nothing              -> ""
        Just rentalActionKey -> borrowerColumnName' rentalActions entityBorrowers rentalActionKey


-- Continuation of borrowerColumnName
borrowerColumnName' :: [RentalAction] -> [Entity Borrower] -> Key RentalAction -> Text
borrowerColumnName' rentalActions entityBorrowers rentalActionKey =
    if(rentalActionLent $ getRentalAction rentalActions rentalActionKey) then
        append ", " (borrowerName $ getBorrower entityBorrowers (rentalActionBorrower $ getRentalAction rentalActions rentalActionKey))
        else
            ""


-- Gets a list of Borrowers and a Borrower Key and returns the Borrower
-- with that key.
getBorrower :: [Entity Borrower] -> Key Borrower -> Borrower
getBorrower entityBorrowers borrowerKey =
    entityVal $ head $ filter (\x -> entityKey x == borrowerKey) entityBorrowers


-- Returns the text for the "Comment" column of the items table. Shows the itemComment
-- unless there is a rentalActionComment specified in the latestRentalAction of the item.
showComment :: [Entity RentalAction] -> Item -> Text
showComment entityRentalActions item =
    showComment'
        (map entityVal entityRentalActions)
        (itemLatestRentalAction item)
        item


-- Continuation of showComment
showComment' :: [RentalAction] -> Maybe (Key RentalAction) -> Item -> Text
showComment' rentalActions maybeRentalActionKey item =
    case maybeRentalActionKey of
        Nothing              ->  (maybe "-" unTextarea (itemItemComment item))
        Just rentalActionKey ->  showComment'' rentalActions rentalActionKey item


-- Continuation of showComment
showComment'' :: [RentalAction] -> Key RentalAction -> Item -> Text
showComment'' rentalActions rentalActionKey item = do
    case (rentalActionLent $ getRentalAction rentalActions rentalActionKey) of
        True    -> (maybe (showComment''' item) unTextarea (rentalActionRentalComment $ getRentalAction rentalActions rentalActionKey))
        False   -> (maybe "-" unTextarea (itemItemComment item))


-- Continuation of showComment
showComment''' :: Item -> Text
showComment''' item = do
    case (itemItemComment item) of
        Nothing            -> "-"
        Just itemComment   -> unTextarea (itemComment)