module Handler.Archive where

import Import
import Database.Persist.Sqlite


-- GET Handler that shows a table of all items and borrowers, that have the
-- "archived" attribute set to "True".
getArchiveR :: Handler Html
getArchiveR = do
    items <- runDB $ selectList [ItemItemArchived ==. True] []
    borrowers <- runDB $ selectList [BorrowerBorrowerArchived ==. True] []
    defaultLayout $ do
        setTitle "Hasventory"
        [whamlet|
            <h2>Archived items
            <table>
                <tr>
                    <th>ID
                    <th>Manufacturer
                    <th>Model
                    <th>Type
                    <th>Actions
                $forall (Entity itemKey item) <- items
                    <tr>
                        <td><span>#{show $ unSqlBackendKey $ toBackendKey itemKey}</span>
                        <td><span>#{itemManufacturer item}</span>
                        <td><span>#{itemModel item}</span>
                        <td><span>#{itemItemType item}</span>
                        <td><ul class="horizontal-list table-row-options">
                            <li><a href="/item/#{show $ unSqlBackendKey $ toBackendKey itemKey}"><img src="/static/img/info-icon.png" alt="details" title="details"></a>
                            <li><a href="/unarchive/item/#{show $ unSqlBackendKey $ toBackendKey itemKey}">unarchive</a>
            <h2>Archived Borrowers
            <table>
                <tr>
                    <th>ID
                    <th>Name
                    <th>E-Mail
                    <th>Actions
                $forall (Entity borrowerKey borrower) <- borrowers
                    <tr>
                        <td><span>#{show $ unSqlBackendKey $ toBackendKey borrowerKey}</span>
                        <td><span>#{borrowerName borrower}</span>
                        <td><span>#{borrowerEmail borrower}</span>
                        <td><ul class="horizontal-list table-row-options">
                            <li><a href="/borrower/#{show $ unSqlBackendKey $ toBackendKey borrowerKey}"><img src="/static/img/info-icon.png" alt="details" title="details"></a>
                            <li><a href="/unarchive/borrower/#{show $ unSqlBackendKey $ toBackendKey borrowerKey}">unarchive</a>
        |]
